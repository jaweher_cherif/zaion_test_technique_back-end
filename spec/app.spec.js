const {convertToMinutes,findLowestWeightNode,getLastTrains} = require('../functions');
const{earliestArrive}=require('../index');

// test convertToMinutes
describe("Convert time HH:MM to minutes", function() {
    it("returns value in minutes", function() {
      expect(convertToMinutes("02:30")).toBe(150);
    });
  });
  
// test findLowestWeightNode
describe("Find Lowest Weight Node (with lowest duration)", function() {
    it("returns lowest weight", function() {
      expect(findLowestWeightNode(
        {
            Paris: { weight: 0, previousNode: '' },
            Amsterdam: { weight: Infinity, previousNode: '' },
            Bruxelles: { weight: Infinity, previousNode: '' },
            Berlin: { weight: Infinity, previousNode: '' }
        },
        new Set ([ 'Paris', 'Amsterdam', 'Bruxelles', 'Berlin' ])
      )).toBe('Paris');
    });

    it("returns lowest weight which is unvisited", function() {
        expect(findLowestWeightNode(
          {
              Paris: { weight: 50, previousNode: '' },
              Amsterdam: { weight: 60, previousNode: '' },
              Bruxelles: { weight: Infinity, previousNode: '' },
              Berlin: { weight: Infinity, previousNode: '' }
          },
          new Set ([ 'Amsterdam', 'Bruxelles', 'Berlin' ])
        )).toBe('Amsterdam');
      });
  });

// test getLastTrains
describe("Get the time of last train from each station", function() {
    it("returns an object that contains for each station the time of last train", function() {
      expect(getLastTrains({
        Paris: { Amsterdam: '09:20', Bruxelles: '08:30' },
        Bruxelles: { Amsterdam: '10:00', Berlin: '11:30' },
        Amsterdam: { Berlin: '12:30' }
      },  new Set ([ 'Paris','Amsterdam', 'Bruxelles','Berlin' ]))
      ).toEqual({ Paris: 560, Amsterdam: 750, Bruxelles: 690 });
    });
  });

  // test earliestArrive
describe("the earliest Arrive time in HH:MM if exists", function() {
    it("returns 18:40", function() {
      expect(earliestArrive(
        "08:20;Paris;Berlin",
        [
            "09:20;Paris;Amsterdam;03:20",
            "08:30;Paris;Bruxelles;01:20",
            "10:00;Bruxelles;Amsterdam;02:10",
            "12:30;Amsterdam;Berlin;06:10",
            "11:30;Bruxelles;Berlin;09:20"
        ]
      )).toBe("18:40");
    });
    it("returns 'No possible way' because jones arrives late at the station", function() {
        expect(earliestArrive(
            "11:20;Paris;Berlin",
            [
                "09:20;Paris;Amsterdam;03:20",
                "08:30;Paris;Bruxelles;01:20",
                "10:00;Bruxelles;Amsterdam;02:10",
                "12:30;Amsterdam;Berlin;06:10",
                "11:30;Bruxelles;Berlin;09:20"
            ]
            
        )).toBe("No possible way");
      });

      it("returns 'No possible way' because jones missed the second train", function() {
        expect(earliestArrive(
            "09:20;Paris;Berlin",
            [
                "09:20;Paris;Amsterdam;03:20",
                "08:30;Paris;Bruxelles;01:20",
                "10:00;Bruxelles;Amsterdam;02:10",
                "12:30;Amsterdam;Berlin;06:10",
                "11:30;Bruxelles;Berlin;09:20"
            ]       
        )).toBe("No possible way");
      });
    
      it("returns 17:40", function() {
        expect(earliestArrive(
            "08:21;Paris;Amsterdam",
            [
                "09:20;Paris;Amsterdam;02:00",
                "08:30;Paris;Bruxelles;01:20",
                "10:00;Bruxelles;Amsterdam;02:10",
                "11:30;Amsterdam;Berlin;06:10",
                "11:30;Bruxelles;Berlin;09:20"
            ]     
        )).toBe("11:20");
      });
  });


