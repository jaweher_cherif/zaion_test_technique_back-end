// **************************** functions ****************************

const {convertToMinutes,findLowestWeightNode,getLastTrains} = require('./functions');
const prepareData = (path,trainInformations) => {
    //get informations from path
    let trip = path.split(";");
    let arriveToStation = convertToMinutes(trip[0]);
    let start = trip [1];
    // prepare graphDuration && graphStart && unvisited
    trainInformations.forEach(element => {
        let array = element.split(";");
        const startTime = array[0];
        const from = array[1];
        const to = array[2];
        const duration = convertToMinutes(array[3]);
        unvisited.add(from);
        unvisited.add(to);
        if( graphDuration[from] == undefined ){
            graphDuration[from]= {};
            graphDuration[from][to]=duration;
            graphStart[from]= {};
            graphStart[from][to]= startTime;
        }
        else{
            graphDuration[from][to]=duration;
            graphStart[from][to]=startTime;
        } 
    });
    // prepare nodes (the table of Dijkstra algorithm)
    unvisited.forEach(element => {
        if (element == start) {
            nodes[element] = {weight: arriveToStation};
        }
        else{
            nodes[element] = {weight: Infinity};
        }
        
    });
    // prepare last train from each node  
    lastTrains=getLastTrains(graphStart,unvisited);  
}

const checkIfTrainIsReachable = (currentNode, element, neighbors) =>{
    let departTime= convertToMinutes(graphStart[currentNode][element]);
    if( nodes[currentNode].weight > departTime){
        return false;
    }
    let arriveTime = convertToMinutes(graphStart[currentNode][element]) + neighbors[element];
    let lastTrain = lastTrains[element];
    if (lastTrain == undefined){
        return true;
    }
    if(arriveTime < lastTrain){
        return true;
    }
    return false;  
};

const earliestArrive = (path,trainInformations) => {
    prepareData(path,trainInformations);

    let trip = path.split(";");
    const finish = trip [2];
    
    while( unvisited.size ){
        let currentNode = findLowestWeightNode(nodes,unvisited);
        let neighbors = graphDuration[currentNode];
        if ( neighbors != undefined && nodes[currentNode].weight!=Infinity){
            Object.keys(neighbors).forEach(element => {
                if(checkIfTrainIsReachable(currentNode, element, neighbors)){
                    let totalWeight = convertToMinutes(graphStart[currentNode][element]) + neighbors[element] ;
                    if ( totalWeight < nodes[element].weight ){
                        nodes[element].weight = totalWeight;
                    }
                }
            });
        }
        unvisited.delete(currentNode);
    }
    if (nodes[finish].weight == Infinity){
        return("No possible way");
    }
    else{
        let HH = Math.floor( nodes[finish].weight / 60 );
        let MM = nodes[finish].weight % 60;
        HH =  HH < 10 ? "0" + HH : HH;
        MM =  MM < 10 ? "0" + MM : MM;
        return( HH + ":" + MM );
    }
}



// **************************** end functions ****************************

// **************************** variables ****************************
let graphDuration = {};
let graphStart = {};
let unvisited = new Set ();
let nodes = [];
let lastTrains = {};
// **************************** end variables ****************************

// **************************** main ****************************



let readline = require('readline');
let path="";
let trainInformations=[];

let rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

rl.prompt();

let lineNumber = 0;
let number=0;
rl.on('line', function (cmd) {

    lineNumber ++;
    if(lineNumber == 1){
        path=cmd;
    }
    else if (lineNumber == 2){
        number=parseInt(cmd);
    }
    else{
        trainInformations.push(cmd);
        if(trainInformations.length==number){
            console.log(earliestArrive(path,trainInformations));
            process.exit(0);       
        }
    }
});

module.exports={
   earliestArrive
}
