
const convertToMinutes = (duration) => {
    let hour = parseInt(duration.substring(0,2));
    let min = parseInt(duration.substring(3,5));
    return hour * 60 + min;
}
  
const findLowestWeightNode = (nodes,unvisited) => {
    let lowestWeight  = Infinity;
    let lowestNode  =  "";
    unvisited.forEach(element => {
        if ( nodes[element].weight <= lowestWeight ){
            lowestWeight = nodes[element].weight;
            lowestNode = element;
        }
    });
    return lowestNode;
} 

const getLastTrains = (graphStart,unvisited) => {
    var lastTrains={}
    unvisited.forEach(node => {
        let last = 0;
        let train=graphStart[node];
        if(train != undefined){
            Object.keys(train).forEach(element => {
                let time=convertToMinutes(train[element]);
                if (time>last){
                    last=time;
                }
            });
            lastTrains[node]=last;
        }
    });   
    return lastTrains;
} 

module.exports={
    findLowestWeightNode,
    convertToMinutes,
    getLastTrains
}
